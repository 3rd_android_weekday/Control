package com.kshrd.control;

import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SecondActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {


    @BindView(R.id.rgGender)
    RadioGroup rgGender;

    @BindView(R.id.cbPhp)
    CheckBox cbPhp;

    @BindView(R.id.cbVb)
    CheckBox cbVb;

    @BindView(R.id.cbCSharp)
    CheckBox cbCSharp;

    @BindView(R.id.tvInfo)
    TextView tvInfo;

    private List<String> subjectList;
    private static final String TAG = "ooooo";
    private String gender = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        ButterKnife.bind(this);

        // Initialize
        subjectList = new ArrayList<>();

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {


                switch (checkedId) {
                    case R.id.rbMale:
                        gender = "Male";
                        break;
                    case R.id.rbFemale:
                        gender = "Female";
                        break;
                }
                Toast.makeText(SecondActivity.this, gender, Toast.LENGTH_SHORT).show();
            }
        });

        cbPhp.setOnCheckedChangeListener(this);
        cbVb.setOnCheckedChangeListener(this);
        cbCSharp.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton view, boolean isChecked) {
        String subject = view.getText().toString();
        if (isChecked) {
            subjectList.add(subject);
        } else {
            subjectList.remove(subject);
        }

//        switch (view.getId()){
//            case R.id.cbPhp:
//                break;
//            case R.id.cbVb:
//                break;
//            case R.id.cbCSharp:
//                break;
//        }
    }

    @OnClick(R.id.btnSubmit)
    public void onSubmit(View v) {

        StringBuilder str = new StringBuilder();
        str.append("Gender -> " + gender + "\n" + "Subjects : ");

        for (String s : subjectList) {
            str.append(s + ",");
        }
        tvInfo.setText(str.substring(0, str.length() - 1));

    }
}
