package com.kshrd.control;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.kshrd.control.model.Country;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CountrySpinnerActivity extends AppCompatActivity {

    @BindView(R.id.spCountry)
    Spinner spCountry;

    int check = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_spinner);
        ButterKnife.bind(this);

        List<Country> countryList = new ArrayList<>();
        countryList.add(new Country(1, "Cambodia"));
        countryList.add(new Country(2, "US"));
        countryList.add(new Country(3, "VN"));
        countryList.add(new Country("--Select--"));

        ArrayAdapter<Country> adapter = new ArrayAdapter<Country>(
                this, android.R.layout.simple_list_item_1, countryList
        ){
            @Override
            public int getCount() {
                int count = super.getCount();
                return (count > 0) ? (count - 1) : count;
            }
        };

        spCountry.setAdapter(adapter);
        spCountry.setSelection(adapter.getCount());

        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (++check > 1){
                    Country country = (Country) parent.getSelectedItem();
                    Toast.makeText(CountrySpinnerActivity.this, String.valueOf(country.getId()), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }
}
